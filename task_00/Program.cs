﻿using System;

namespace task_00
{
    class User
    {
        public string Login { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        DateTime Date { get; set; }

        public User(string login)
        {
            Login = login;
            Date = DateTime.Now;
        }

        public void ShowUserInfo()
        {
            Console.WriteLine("Login: {0}", Login);

            Console.WriteLine("Name: {0}", !String.IsNullOrEmpty(Name) ? Name : "no name provided");
            Console.WriteLine("Surname: {0}", !String.IsNullOrEmpty(Surname) ? Surname : "no surname provided");
            Console.WriteLine("Age: {0}", Age != 0 ? Age.ToString() : "no age provided");

            Console.WriteLine("Date of creation: {0}", Date);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            User user = new User("cheburashka");

            user.Name = "Vasia";
            user.Age = 30;

            user.ShowUserInfo();

            Console.ReadKey();
        }
    }
}
