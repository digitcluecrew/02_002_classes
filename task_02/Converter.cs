﻿namespace task_02
{
    class Converter
    {
        double UsdRate { get; set; }
        double EurRate { get; set; }
        double RubRate { get; set; }

        public Converter(double usd, double eur, double rub)
        {
            UsdRate = usd;
            EurRate = eur;
            RubRate = rub;
        }

        public double Convert(double money, CURRENCY currency = CURRENCY.USD)
        {
            switch (currency)
            {
                case CURRENCY.USD:
                    {
                        return ConvertUsd(money);
                    }
                case CURRENCY.EUR:
                    {
                        return ConvertEur(money);
                    }
                case CURRENCY.RUB:
                    {
                        return ConvertRub(money);
                    }
                default:
                    {
                        return 0;
                    }
            }
        }

        public double ConvertTo(double money, CURRENCY currency = CURRENCY.USD)
        {
            switch (currency)
            {
                case CURRENCY.USD:
                    {
                        return ConvertToUsd(money);
                    }
                case CURRENCY.EUR:
                    {
                        return ConvertToEur(money);
                    }
                case CURRENCY.RUB:
                    {
                        return ConvertToRub(money);
                    }
                default:
                    {
                        return 0;
                    }
            }
        }

        public double ConvertUsd(double usd)
        {
            return usd / UsdRate;
        }

        public double ConvertEur(double eur)
        {
            return eur / EurRate;
        }

        public double ConvertRub(double rub)
        {
            return rub / RubRate;
        }

        public double ConvertToUsd(double hrivna)
        {
            return hrivna * UsdRate;
        }

        public double ConvertToEur(double hrivna)
        {
            return hrivna * EurRate;
        }

        public double ConvertToRub(double hrivna)
        {
            return hrivna * RubRate;
        }
    }
}
