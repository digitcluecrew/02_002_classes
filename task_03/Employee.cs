﻿namespace task_03
{
    class Employee
    {
        double baseSalary = 1000;
        double RankRate
        {
            get
            {
                switch (Rank)
                {
                    case RANK.MIDDLE:
                        return 1.6;
                    case RANK.SENIOR:
                        return 2.1;
                }

                return 1;
            }
        }

        double ExpRate
        {
            get
            {
                if (Experience > 10)
                {
                    return 2;
                }
                else if (Experience > 5)
                {
                    return 1.7;
                }
                else if (Experience > 2)
                {
                    return 1.3;
                }

                return 1;
            }
        }

        public string Name { get; set; }
        public string Surname { get; set; }

        public RANK Rank { get; set; }

        public int Experience { get; set; }

        public double Salary
        {
            get
            {
                return baseSalary * RankRate * ExpRate;
            }
        }

        public double Tax
        {
            get
            {
                return Salary * 0.05;
            }
        }

        Employee()
        {
            Rank = RANK.JUNIOR;
            Experience = 0;
        }

        public Employee(string name, string surname)
            : this()
        {
            Name = name;
            Surname = surname;
        }
    }
}
