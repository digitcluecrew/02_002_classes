﻿using System;

namespace task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] emps =
            {
                new Employee("Lida", "Kalina")
                {
                    Experience = 1
                },
                new Employee("Vasia", "Pupkin")
                {
                    Rank = RANK.MIDDLE,
                    Experience = 3
                },
                new Employee("Klim", "Stroustrup")
                {
                    Rank = RANK.SENIOR,
                    Experience = 10
                }
            };


            for (int i = 0; i < emps.Length; i++)
            {
                Console.WriteLine("-- {0} {1} --", emps[i].Name, emps[i].Surname);
                Console.WriteLine("Rank: {0}", emps[i].Rank);
                Console.WriteLine("Experience: {0}", emps[i].Experience);
                Console.WriteLine("Salary: {0}", emps[i].Salary);
                Console.WriteLine("Tax: {0}", emps[i].Tax);
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
